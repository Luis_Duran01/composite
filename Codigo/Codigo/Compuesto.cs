﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Codigo
{
    class Compuesto<T> : IComponente<T>
    {
        List<IComponente<T>> elementos;

        public T Nombre { get; set; }
        
        public Compuesto(T pNombre)
        {
            Nombre = pNombre;

            elementos = new List<IComponente<T>>();
        }
        public void Adicionar(IComponente<T> pElementos)
        {
            elementos.Add(pElementos);
        }
        public IComponente<T> Borrar(T pElementos)
        {
            IComponente<T> elemento = this.Buscar(pElementos);

            if(elemento != null)
            {
                (this as Compuesto<T>).elementos.Remove(elemento);
            }
            return this;
        }
        public IComponente<T> Buscar(T pElemento)
        {
            if (Nombre.Equals(pElemento))
                return this;
            IComponente<T> encontrado = null;

            foreach(IComponente<T> elemento in elementos)
            {
                encontrado = elemento.Buscar(pElemento);
                if (encontrado != null)
                    break;
            }
            return this;
        }
        public string Mostrar(int pProfundidad)
        {
            //construimos la cadena con una cantidad de - igual a la profundidad 
            StringBuilder infoElemento = new StringBuilder(new String('-', pProfundidad));

            //adicionalmete la informacion del compuesto 
            infoElemento.Append("Compuesto: " + Nombre + " elementos: " + elementos.Count + "\n\r");

            // adicionalmente los elementos
            foreach (IComponente<T> elemento in elementos)
                infoElemento.Append(elemento.Mostrar(pProfundidad + 1));

            return infoElemento.ToString();
        }
    }
}
