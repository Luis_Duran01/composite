﻿using System;

namespace Codigo
{
    class Program
    {
        static void Main(string[] args)
        {
            IComponente<string> arbol = new Compuesto<string>("root ");
            IComponente<string> trabajo = arbol;

            string opcion = "";
            string dato = "";

            while(opcion != "6")
            {
                Console.WriteLine("Estoy en {0}", trabajo.Nombre);
                Console.WriteLine("1-Adicionar compuesto 2-Adicionar componete 3-Eliminar 4-Buscar 5-Mostrar 6-Salir");
                opcion = Console.ReadLine();
                Console.WriteLine("--------");

                if (opcion == "1")
                {
                    Console.WriteLine("Dame el número del compuesto: ");
                    dato = Console.ReadLine();
                    IComponente<string> c = new Compuesto<string>(dato);
                    trabajo.Adicionar(c);
                    trabajo = c;

                }
                if(opcion == "2")
                {
                    Console.WriteLine("Dame el nombre del componente: ");
                    dato = Console.ReadLine();
                    trabajo.Adicionar(new Componente<string>(dato));
                }
                if(opcion == "3")
                {
                    Console.WriteLine("Dame el elemeneto a borrar: ");
                    dato = Console.ReadLine();
                    trabajo = trabajo.Borrar(dato);
                }
                if(opcion == "4")
                {
                    Console.WriteLine("dame el elemento a encontrar: ");
                    dato = Console.ReadLine();
                    trabajo = trabajo.Buscar(dato);
                }
                if(opcion == "5")
                {
                    Console.WriteLine(arbol.Mostrar(0));
                }
                   
            }
        }
    }
}
