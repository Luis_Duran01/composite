﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Codigo
{
    interface IComponente<T>
    {
        T Nombre{get ; set;}
        void Adicionar(IComponente<T> pElemento);
        IComponente<T> Borrar(T pElementos);
        IComponente<T> Buscar(T pElementos);
        string Mostrar(int pProfundidades);
    }
}
